
// Use the "require" directive to load the express module/package
const express = require("express")

//Create an application express
const app = express()


// For our application server to run, we need a port to listen to
const port = 3000


//Allows app to read json data
app.use(express.json())

// Allow your app to read other data types
app.use(express.urlencoded({extended: true}))


// Routes 
//Express has methods corresponding to each HTTP method

// GET
app.get("/greet",(request, response) => {
	response.send('Hello from the /greet endpoint')
})

// POST
app.post("/hello",(request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
})

// Simple registration form

// mock database
let users = []

app.post("/signup",(request,response) => {
	if(request.body.username !== '' && request.body.username !== undefined && request.body.password !== '' && request.body.password !== undefined){
		users.push(request.body)
		response.send(`User ${request.body.username} successfully registered.`)

	}
	else
	{
		response.send(`Please input BOTH username and password`)
	}
})

// Change Password
app.patch("/change-password",(request,response) => {

	let message

	for(let i = 0; i<users.length;i++){
		if(request.body.username == users[i].username )
		{
		
			users[i].password = request.body.password
			message = `User ${request.body.username}'s password has been updated.`


			
		}
		else
		{
			message = `User does not exist`

		}
	}

	
	response.send(message)
})




//Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`))







const express = require("express")

const app = express()


const port = 4500



app.use(express.json())


app.use(express.urlencoded({extended: true}))

app.get("/home",(request, response) => {
	response.send('Welcome to home page')
})

let users = [
	{
		"username": "sample",
		"password": "sample"
	},
	{
		"username": "sample1",
		"password": "sample1"
	}
]
app.get("/users",(request, response) => {
	

	response.send(users)
})


app.delete("/delete-user",(request, response) => {
	
	let message

	if(users.find(username => username.username === request.body.username)){
			message = `User ${request.body.username} has been deleted`
		
		let indexofuser = users.map(user => user.username).indexOf(request.body.username)
		users.splice(indexofuser,1)
	

	}
	else {
		message = `User does not exist`
	}
	response.send(message)
})


app.listen(port, () => console.log(`Server running at port ${port}`))